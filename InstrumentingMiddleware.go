package instrumentingmiddleware

import (
	"context"
	"errors"
	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/metrics"
	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
	stdprometheus "github.com/prometheus/client_golang/prometheus"
	//"log"
	//"strconv"
	"time"
)

var (
	ErrInconsistentIDs = errors.New("inconsistent IDs")
	ErrAlreadyExists   = errors.New("already exists")
	ErrNotFound        = errors.New("not found")
)

var (
	fieldKeys = []string{"method"}

	RequestCount = kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
		Namespace: "kraken",
		Subsystem: "service",
		Name:      "request_count",
		Help:      "Number of requests received.",
	}, fieldKeys)

	RequestLatency = kitprometheus.NewHistogramFrom(stdprometheus.HistogramOpts{
		Namespace: "kraken",
		Subsystem: "service",
		Name:      "request_latency_microseconds",
		Help:      "Total duration of requests in microseconds.",
	}, fieldKeys)

	Requesthttp = kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
		Namespace: "kraken",
		Subsystem: "service",
		Name:      "request_count_status_code",
		Help:      "Number of Request per status code",
	}, []string{"status_code"},
	)
)

type instrumentingMiddleware struct {
	RequestCount   metrics.Counter
	RequestLatency metrics.Histogram
	Requesthttp    metrics.Counter
}

func NewInstrumentingMiddleware(RequestCount metrics.Counter, RequestLatency metrics.Histogram, Requesthttp metrics.Counter, methodname string) endpoint.Middleware {
	mw := instrumentingMiddleware{RequestCount: RequestCount, RequestLatency: RequestLatency}

	return func(next endpoint.Endpoint) endpoint.Endpoint {
		return func(c context.Context, request interface{}) (interface{}, error) {
			lvs := []string{"method", methodname}
			defer func(begin time.Time) {
				mw.RequestCount.With(lvs...).Add(1)
				mw.RequestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
			}(time.Now())

			p, err := next(c, request)
			return p, err
		}
	}
}
